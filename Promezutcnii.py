import sim  # V-rep library
import sys

from MyKopter import MyKopterBot
from MyMasterBotControl import MyBotDownControl


#cv2.namedWindow("result")
class Control(object):

    def __init__(self,RF):
        if RF:
            sim.simxFinish(-1)  # just in case, close all opened connections

            clientID = sim.simxStart('127.0.0.1', 19999, True, True, 5000, 5)

            if clientID != -1:  # check if client connection successful
                print('Connected to remote API server')

            else:
                print('Connection not successful')
                sys.exit('Could not connect')

            self.clientId = clientID
            self.MB = MyBotDownControl(clientID)
            self.MC = MyKopterBot(clientID)
            self.ReOfWork=RF##если правда то модель иначе реальны
        else:
            self.ReOfWork=RF

            #вставить необходимые команды
            print('Real')

    def chassisMove(self, x, y, z):
        if self.ReOfWork:
            self.MB.chassisMove(x, y, z)
        else:
            print('movereal')

    def chassisSetSpeed(self,Vx,Vy,Vz):
        if self.ReOfWork:
            self.MB.chassisSetSpeed(Vx,Vy,Vz)
        else:
            print('Setvelositureal')



    def roboticArmMoveTo(self,x,y):
        if self.ReOfWork:
            self.MB.roboticArmMoveTo(x,y)
        else:
            print('moverealManipulator')

    def roboticArmMove(self,dx,dy):# перемещение манипулятора на +х + у
        if self.ReOfWork:
            self.MB.roboticArmMove(dx,dy)
        else:
            print('moverealManipulator')

    def roboticArmRecenter(self):
        if self.ReOfWork:
            self.MB.roboticArmRecenter()
        else:
            print('Realrecenter')

    def roboticArmOptimalForward(self):
        if self.ReOfWork:
            self.MB.roboticArmOptimalForward()
        else:
            print('Realrecenter')


    def roboticArmStop(self):
        if self.ReOfWork:
            self.MB.roboticArmStop()
        else:
            print('RealStop')


    def roboticArmReturnPosition(self):
        if self.ReOfWork:
            self.MB.roboticArmReturnPosition()
        else:
            print('RealReturnPos')

    def roboticGripperClose(self,force):
        print('returnpos')

    def roboticGripperOpen(self,force):
        print('returnpos')

    def roboticGripperStatus(self):
        print('returnpos')

    def dronGo(self,x,y,z):#пермещение коптаера на +впереда на х, вправо на у вверех на з, поворот на flpha
        if self.ReOfWork:
            self.MC.kopMove(x,y,z,0)
        else:
            print('moverealKopter')

    def dronUp(self,d):
        if self.ReOfWork:
            self.MC.dronUp(d)
        else:
            print('moverealKopter')

    def dronDown(self, d):
        if self.ReOfWork:
            self.MC.dronDown(d)
        else:
            print('moverealKopter')

    def dronLeft(self, d):
        if self.ReOfWork:
            self.MC.dronLeft(d)
        else:
            print('moverealKopter')

    def dronRight(self, d):
        if self.ReOfWork:
            self.MC.dronRight(d)
        else:
            print('moverealKopter')

    def dronFoward(self, d):
        if self.ReOfWork:
            self.MC.dronFoward(d)
        else:
            print('moverealKopter')

    def dronBack(self, d):
        if self.ReOfWork:
            self.MC.dronBack(d)
        else:
            print('moverealKopter')

    def dronRotateRight(self, d):
        if self.ReOfWork:
            self.MC.dronRotateRight(d)
        else:
            print('moverealKopter')

    def dronRotateLeft(self, d):
        if self.ReOfWork:
            self.MC.dronRotateLeft(d)
        else:
            print('moverealKopter')




if __name__ == '__main__':
    Model=True
    Co=Control(Model)
    Co.MovOTN(1,0,0.5)
